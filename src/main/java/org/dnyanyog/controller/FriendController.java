package org.dnyanyog.controller;

import org.dnyanyog.dto.request.CreateFriendRequest;
import org.dnyanyog.dto.response.CreateFriendResponse;
import org.dnyanyog.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FriendController {
	@Autowired
	FriendService service;
	
	@PostMapping(path = "friends/api/v1/create",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<CreateFriendResponse> createFriend(@RequestBody CreateFriendRequest request) {
		
		return service.createFriend(request);
	}
}
