package org.dnyanyog.controller;

import javax.validation.Valid;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.GetUserListResponse;
import org.dnyanyog.dto.response.GetUserResponse;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.service.DirectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DirectoryController {

	@Autowired
	DirectoryService service;
	


	@PostMapping(path = "directory/api/v1/signup",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<SignUpResponse> signUpUser(@RequestBody @Valid SignUpRequest request) {
	
		return service.signUp(request);
	}
	
	
	@PostMapping(path="directory/api/v1/validate",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {

		return service.login(request);
	}	
	
	@GetMapping(path="directory/api/v1/user/{userId}")
	public GetUserResponse getUserById(@PathVariable long userId) {
		
		return service.getUserById(userId);
	}
	
	@GetMapping(path="directory/api/v1/users")
	public  GetUserListResponse getUser() {
		
		return service.getUser();
	}
}
