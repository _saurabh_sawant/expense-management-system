package org.dnyanyog.controller;

import org.dnyanyog.dto.request.CreateGroupRequest;
import org.dnyanyog.dto.request.GroupUserMapRequest;
import org.dnyanyog.dto.response.CreateGroupResponse;
import org.dnyanyog.dto.response.GroupUserMapResponse;
import org.dnyanyog.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController {
	@Autowired
	GroupService service;
	
	@PostMapping(path="groups/api/v1/create",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<CreateGroupResponse> createGroup(@RequestBody CreateGroupRequest request) {
		
		return service.createGroup(request);
	}
	
	@PostMapping(path="groups/api/v1/friends",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<GroupUserMapResponse> groupUserMap(@RequestBody GroupUserMapRequest request) {
		
		return service.groupUserMap(request);
	}
}
