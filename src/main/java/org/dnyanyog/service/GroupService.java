package org.dnyanyog.service;

import org.dnyanyog.dto.request.CreateGroupRequest;
import org.dnyanyog.dto.request.GroupUserMapRequest;
import org.dnyanyog.dto.response.CreateGroupResponse;
import org.dnyanyog.dto.response.GroupUserMapResponse;
import org.dnyanyog.entity.GroupInfo;
import org.dnyanyog.entity.GroupUserMap;
import org.dnyanyog.repository.GroupRepository;
import org.dnyanyog.repository.GroupUserMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GroupService {
	@Autowired
	GroupRepository groupRepo;
	@Autowired 
	GroupInfo group;
	@Autowired 
	CreateGroupResponse createGroupResponse;
	@Autowired
	GroupUserMapRepository groupUserMapRepo;
	@Autowired
	GroupUserMap groupUserMap;
	@Autowired
	GroupUserMapResponse groupUserMapResponse; 
	
	public long set;

	public ResponseEntity<CreateGroupResponse> createGroup(CreateGroupRequest gRequest) {

		group.setGroupName(gRequest.getGroupName());
		group.setType(gRequest.getType());

		group = groupRepo.save(group);

		createGroupResponse.setStatus("Success");
		createGroupResponse.setMessage("Group Created Successfully");

		createGroupResponse.setGroupId(group.getGroupId());
		createGroupResponse.setGroupName(group.getGroupName());
		createGroupResponse.setType(group.getType());
		createGroupResponse.setUserId(set);
		
		System.out.println("Set Id group is  "+set);

		return ResponseEntity.status(HttpStatus.CREATED).body(createGroupResponse);
	}
	
	public ResponseEntity<GroupUserMapResponse> groupUserMap(GroupUserMapRequest request) {

		groupUserMap.setUserId(request.getUserId());
		groupUserMap.setGroupId(request.getGroupId());

		groupUserMap = groupUserMapRepo.save(groupUserMap);

		groupUserMapResponse.setStatus("Success");
		groupUserMapResponse.setMessage("Friend Added Successfully to the group");

		groupUserMapResponse.getData().setUserId(groupUserMap.getUserId());
		groupUserMapResponse.getData().setGroupId(groupUserMap.getGroupId());

		return ResponseEntity.status(HttpStatus.CREATED).body(groupUserMapResponse);
	}
}
