package org.dnyanyog.service;

import java.util.ArrayList;
import java.util.List;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.GetUserData;
import org.dnyanyog.dto.response.GetUserListResponse;
import org.dnyanyog.dto.response.GetUserResponse;
import org.dnyanyog.dto.response.LoginError;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpData;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DirectoryService {

	@Autowired
	UserRepository userRepo;
	@Autowired
	Users user;
	@Autowired
	List<Users> listUser;
	@Autowired
	SignUpResponse signUpResponse;
	@Autowired
	GetUserResponse getUserResponse;
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	LoginError fails;
	@Autowired
	GetUserListResponse getUserListResponse;
	@Autowired
	List<Users> userList;
	@Autowired
	List<GetUserData> getUserDataList;
	@Autowired
	GetUserData getUserData;
	@Autowired
	GroupService gs;
	
	//public long set;
	
	@Transactional
	public ResponseEntity<SignUpResponse> signUp(SignUpRequest request) {

		signUpResponse = new SignUpResponse();
		signUpResponse.setData(new SignUpData());
		user = new Users();

		if (null != userRepo.findByEmail(request.getEmail())
				&& null != userRepo.findByEmail(request.getEmail()).getPassword())
			return getConflictSignUpResponse();

		if (null != userRepo.findByMobile(request.getMobile())
				&& null != userRepo.findByMobile(request.getMobile()).getPassword())
			return getConflictSignUpResponse();

		if (null != userRepo.findByEmail(request.getEmail())
				&& null == userRepo.findByEmail(request.getEmail()).getPassword()) {
			user = userRepo.findByEmail(request.getEmail());
		}

		if (null != userRepo.findByMobile(request.getMobile())
				&& null == userRepo.findByMobile(request.getMobile()).getPassword()) {
			user = userRepo.findByMobile(request.getMobile());
		}

		user.setCurrency(request.getCurrency());
		user.setCountry(request.getCountry());
		user.setFullName(request.getFullName());
		user.setEmail(request.getEmail());
		user.setLanguage(request.getLanguage());
		user.setMobile(request.getMobile());
		user.setPassword(request.getPassword());

		user = userRepo.save(user);

		signUpResponse.setStatus("success");
		signUpResponse.setMessage("user account created successfully");
		signUpResponse.getData().setUserId(user.getUserId());
		signUpResponse.getData().setCountry(user.getCountry());
		signUpResponse.getData().setCurrency(user.getCurrency());
		signUpResponse.getData().setEmail(user.getEmail());
		signUpResponse.getData().setFullName(user.getFullName());
		signUpResponse.getData().setLanguage(user.getLanguage());
		signUpResponse.getData().setMobile(user.getMobile());

		return ResponseEntity.status(HttpStatus.CREATED).body(signUpResponse);
	}

	public ResponseEntity<LoginResponse> login(LoginRequest request) {
		loginResponse = new LoginResponse();
		loginResponse.setData(new SignUpData());
		loginResponse.setErrors(new LoginError());
		if (null != userRepo.findByEmail(request.getEmail()) && null != userRepo.findByMobile(request.getMobile())
				&& null != userRepo.findByPassword(request.getPassword())) {

			user.setEmail(request.getEmail());
			user.setMobile(request.getMobile());
			user.setPassword(request.getPassword());

			user = userRepo.findByEmail(request.getEmail());
			user = userRepo.findByMobile(request.getMobile());
			user = userRepo.findByPassword(request.getPassword());

			loginResponse.setStatus("success");
			loginResponse.setMessage("Validation successfull");
			gs.set= loginResponse.getData().setUserId(user.getUserId());
			loginResponse.getData().setCountry(user.getCountry());
			loginResponse.getData().setCurrency(user.getCurrency());
			loginResponse.getData().setEmail(user.getEmail());
			loginResponse.getData().setFullName(user.getFullName());
			loginResponse.getData().setLanguage(user.getLanguage());
			loginResponse.getData().setMobile(user.getMobile());

			System.out.println("Set Id is  "+gs.set);
			System.out.println("Login Successfully");
			
			loginResponse.setErrors(null);

			return ResponseEntity.status(HttpStatus.CREATED).body(loginResponse);
		}

		loginResponse.setStatus("error");
		loginResponse.setMessage("Validation failed");
		loginResponse.setData(null);

		loginResponse.getErrors().setField("email");
		loginResponse.getErrors().setMessage("Email is not valid");
		loginResponse.getErrors().setMessage("mobile");
		loginResponse.getErrors().setMessage("Mobile number is required");

		return ResponseEntity.status(HttpStatus.CONFLICT).body(loginResponse);

	}

	public GetUserResponse getUserById(long userId) {
		user = userRepo.findById(userId).orElse(null);

		getUserResponse.setStatus("success");
		getUserResponse.setMessage("Data fetch successful");
		getUserResponse.getData().setUserId(user.getUserId());
		getUserResponse.getData().setCountry(user.getCountry());
		getUserResponse.getData().setCurrency(user.getCurrency());
		getUserResponse.getData().setEmail(user.getEmail());
		getUserResponse.getData().setFullName(user.getFullName());
		getUserResponse.getData().setLanguage(user.getLanguage());
		getUserResponse.getData().setMobile(user.getMobile());

		return getUserResponse;
	}

	public GetUserListResponse getUser() {
		userList = userRepo.findAll();
		getUserDataList = new ArrayList<>();

		for (Users user : userList) {

			GetUserData getUserData = new GetUserData();

			getUserData.setEmail(user.getEmail());
			getUserData.setMobile(user.getMobile());
			getUserData.setUserId(user.getUserId());
			getUserData.setFullName(user.getFullName());
			getUserData.setCurrency(user.getCurrency());
			getUserData.setLanguage(user.getLanguage());
			getUserData.setCountry(user.getCountry());
			getUserDataList.add(getUserData);
		}

		getUserListResponse.setStatus("Success");
		getUserListResponse.setMessage("Data fetch successful");
		getUserListResponse.setData(getUserDataList);

		return getUserListResponse;
	}

	private ResponseEntity<SignUpResponse> getConflictSignUpResponse() {
		SignUpResponse response = new SignUpResponse();
		response.setStatus("error");
		response.setMessage("Email or mobile number already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}

}
