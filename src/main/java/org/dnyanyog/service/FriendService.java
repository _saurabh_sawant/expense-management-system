package org.dnyanyog.service;

import org.dnyanyog.dto.request.CreateFriendRequest;
import org.dnyanyog.dto.response.CreateFriendData;
import org.dnyanyog.dto.response.CreateFriendResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FriendService {
	
	@Autowired
	CreateFriendResponse createFriendResponse;
	@Autowired
	UserRepository userRepo;
	@Autowired
	Users user;
	
	public ResponseEntity<CreateFriendResponse> createFriend(CreateFriendRequest fRequest) {

		createFriendResponse = new CreateFriendResponse();
		createFriendResponse.setData(new CreateFriendData());
		user = new Users();

		if (null != userRepo.findByEmail(fRequest.getEmail()) || null != userRepo.findByMobile(fRequest.getMobile())) {
			
			createFriendResponse.setStatus("error");
			createFriendResponse.setMessage("Email or mobile number already registered");
			createFriendResponse.setData(null);
			
			return ResponseEntity.status(HttpStatus.CONFLICT).body(createFriendResponse);
		}

		user.setFullName(fRequest.getFullName());
		user.setEmail(fRequest.getEmail());
		user.setMobile(fRequest.getMobile());

		user = userRepo.save(user);

		createFriendResponse.setStatus("Success");
		createFriendResponse.setMessage("User Account Created Successfully");
		createFriendResponse.getData().setUserId(user.getUserId());
		createFriendResponse.getData().setFullName(user.getFullName());
		createFriendResponse.getData().setEmail(user.getEmail());
		createFriendResponse.getData().setMobile(user.getMobile());
		createFriendResponse.getData().setCurrency("");
		createFriendResponse.getData().setCountry("");
		createFriendResponse.getData().setLanguage("");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(createFriendResponse);
	}
}
