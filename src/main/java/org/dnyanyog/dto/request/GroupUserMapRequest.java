package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class GroupUserMapRequest {
	private long userId;
	private long groupId;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	
}
