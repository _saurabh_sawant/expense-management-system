package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class CreateGroupRequest {
	private String groupName;
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
