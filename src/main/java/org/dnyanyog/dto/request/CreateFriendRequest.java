package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class CreateFriendRequest {
	private String fullName; 
	private String email;
	private String mobile;
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
