package org.dnyanyog.dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginResponse {
	private String status;
	private String message;
	@Autowired
	private SignUpData data;
	@Autowired
	private LoginError errors;
	
	
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public SignUpData getData() {
		return data;
	}
	public void setData(SignUpData data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public LoginError getErrors() {
		return errors;
	}

	public void setErrors(LoginError errors) {
		this.errors = errors;
	}
}
