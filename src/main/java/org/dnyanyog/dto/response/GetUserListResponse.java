package org.dnyanyog.dto.response;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class GetUserListResponse {
	private String status;
	private String message;
	@Autowired
	private List<GetUserData> data;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<GetUserData> getData() {
		return data;
	}
	public void setData(List<GetUserData> data) {
		this.data = data;
	}
	
}
