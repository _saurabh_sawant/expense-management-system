package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class CreateGroupData {
	private long groupId;
	private String groupName;
	private String type;
	
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
