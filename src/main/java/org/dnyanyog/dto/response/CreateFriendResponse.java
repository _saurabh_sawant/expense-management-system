package org.dnyanyog.dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateFriendResponse {
	private String status;
	private String message;
	@Autowired
	private CreateFriendData data;
	
	public CreateFriendData getData() {
		return data;
	}

	public void setData(CreateFriendData data) {
		this.data = data;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
