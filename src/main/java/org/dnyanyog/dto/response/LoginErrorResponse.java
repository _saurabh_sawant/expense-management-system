package org.dnyanyog.dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginErrorResponse {
	private String status;
	private String message;
	@Autowired
	private LoginError error;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public LoginError getError() {
		return error;
	}
	public void setError(LoginError error) {
		this.error = error;
	}
	
}
