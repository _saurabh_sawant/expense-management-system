package org.dnyanyog.dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GroupUserMapResponse {
	private String status;
	private String message;
	@Autowired
	private GroupUserMapData data;
	
	public GroupUserMapData getData() {
		return data;
	}

	public void setData(GroupUserMapData data) {
		this.data = data;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
