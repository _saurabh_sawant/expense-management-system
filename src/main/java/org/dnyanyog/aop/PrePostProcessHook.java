/*package org.dnyanyog.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.dnyanyog.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class PrePostProcessHook {

	private static final Logger logger = LoggerFactory.getLogger(PrePostProcessHook.class);

	@After("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterExecution(JoinPoint joinPoint) {
		System.out.println("\n************* Saved object - " + joinPoint.getArgs()[0]);

		Users user = (Users) joinPoint.getArgs()[0];
		logger.info(user.getCountry());
		logger.info(user.getCurrency());
		logger.info(user.getEmail());
		logger.info(user.getPassword());
	}

	@Before("execution(* org.dnyanyog.repository.*.save(..))")
	public void beforeExecution(JoinPoint joinPoint) {

		System.out.println("\n************* Saving object - " + joinPoint.getArgs()[0]);

		Users user = (Users) joinPoint.getArgs()[0];
		logger.info(user.getCountry());
		logger.info(user.getCurrency());
		logger.info(user.getEmail());
		logger.info(user.getPassword());
	}
	
	/*
	@Around("execution(* org.dnyanyog.repository.*.save(..))")
	public void aroundExecution(JoinPoint joinPoint) {

		System.out.println("\n************* Around - " + joinPoint.getArgs()[0]);

		Users user = (Users) joinPoint.getArgs()[0];
		logger.info(user.getCountry());
		logger.info(user.getCurrency());
		logger.info(user.getEmail());
		logger.info(user.getPassword());
		if (user.getPassword().length() < 12) {

			logger.info("Password is less than 12 characters");
		}
	}
	
	
	@AfterReturning("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterReturningExecution(JoinPoint joinPoint) {

		System.out.println("\n************* After Returning - " + joinPoint.getArgs()[0]);

		Users user = (Users) joinPoint.getArgs()[0];
		logger.info(user.getCountry());
		logger.info(user.getCurrency());
		logger.info(user.getEmail());
		logger.info(user.getPassword());
	}
	
	@AfterThrowing("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterThrowingExecution(JoinPoint joinPoint) {

		System.out.println("\n************* After Throwing - " + joinPoint.getArgs()[0]);

		Users user = (Users) joinPoint.getArgs()[0];
		logger.info(user.getCountry());
		logger.info(user.getCurrency());
		logger.info(user.getEmail());
		logger.info(user.getPassword());
	}
}*/
