package org.dnyanyog.repository;

import org.dnyanyog.dto.request.CreateGroupRequest;
import org.dnyanyog.entity.GroupInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface GroupRepository extends JpaRepository<GroupInfo, Long>{

	void save(CreateGroupRequest gRequest);

}
