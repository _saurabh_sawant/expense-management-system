package org.dnyanyog.repository;

import org.dnyanyog.entity.GroupUserMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupUserMapRepository extends JpaRepository <GroupUserMap, Long>{

}
