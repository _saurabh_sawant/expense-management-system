package org.dnyanyog.controller;

import javax.xml.xpath.XPathExpressionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest

@AutoConfigureMockMvc

public class DirectoryControllerTest extends AbstractTestNGSpringContextTests {
	@Autowired
	MockMvc mocMvc;

	//New Data
	@Test(priority=1)
	public void verifyUsersSignupSuccessOpertionExceptionObject() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\n"
				+ "<fullName>Saurabh Sawant</fullName>\n"
				+ "<email>saurabh@gmail.com</email>\n"
				+ "<password>Saurabh@123</password>\n" 
				+ "<mobile>8888888888</mobile>\n"
				+ "<currency>Dollar</currency>\n" 
				+ "<language>English</language>\n" 
				+ "<country>US</country>\n"
				+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isCreated())
				.andExpect(xpath("/SignUpResponse/status").string("success"))
				.andExpect(xpath("/SignUpResponse/message").string("user account created successfully"))
				.andExpect(xpath("/SignUpResponse/data/fullName").string("Saurabh Sawant"))
				.andExpect(xpath("/SignUpResponse/data/email").string("saurabh@gmail.com"))
				.andExpect(xpath("/SignUpResponse/data/mobile").string("8888888888"))
				.andExpect(xpath("/SignUpResponse/data/currency").string("Dollar"))
				.andExpect(xpath("/SignUpResponse/data/language").string("English"))
				.andExpect(xpath("/SignUpResponse/data/country").string("US"))
				.andExpect(xpath("/SignUpResponse/data/userId").string("1"))
				.andReturn();
	}

	//Existing Data
	@Test(priority=2)
	public void verifyUsersSignupFailForDuplicateData() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>Saurabh Sawant</fullName>\n"
						+ "<email>saurabh@gmail.com</email>\n"
						+ "<password>Saurabh@123</password>\n"
						+ "<mobile>8888888888</mobile>\n"
						+ "<currency>Dollar</currency>\n"
						+ "<language>English</language>\n"
						+ "<country>US</country>\n" 
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}

	//Existing Email
	@Test(priority=3)
	public void verifyUserSignupFailForDuplicateEmail() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>Saurabh Sawant</fullName>\n"
						+ "<email>saurabh@gmail.com</email>\n"
						+ "<password>Saurabh@123</password>\n"
						+ "<mobile>4545454543</mobile>\n"
						+ "<currency>Dollar</currency>\n"
						+ "<language>English</language>\n"
						+ "<country>US</country>\n" 
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}

	//Existing Mobile
	@Test(priority=3)
	public void verifyUserSignupFailForDuplicateMobile() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>Saurabh Sawant</fullName>\n"
						+ "<email>saurabh72@gmail.com</email>\n"
						+ "<password>Saurabh@123</password>\n"
						+ "<mobile>8888888888</mobile>\n"
						+ "<currency>Dollar</currency>\n"
						+ "<language>English</language>\n"
						+ "<country>US</country>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}
	
	//Existing Mobile & Email but No Password
	@Test(priority=4)
	public void verifyUserSignupSuccessForCreateFriend() throws XPathExpressionException, Exception {
		
		//Create Friend
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/friends/api/v1/create")
				.content("<User>\n"
						+ "<fullName>Gaurav Pandarkar</fullName>\n"
						+ "<email>gaurav@gmail.com</email>\n" 
						+ "<mobile>4444444444</mobile>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
	
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isCreated())
				.andExpect(xpath("/CreateFriendResponse/status").string("Success"))
				.andExpect(xpath("/CreateFriendResponse/message").string("User Account Created Successfully"))
				.andExpect(xpath("/CreateFriendResponse/data/fullName").string("Gaurav Pandarkar"))
				.andExpect(xpath("/CreateFriendResponse/data/email").string("gaurav@gmail.com"))
				.andExpect(xpath("/CreateFriendResponse/data/mobile").string("4444444444"))
				.andExpect(xpath("/CreateFriendResponse/data/currency").string(""))
				.andExpect(xpath("/CreateFriendResponse/data/language").string(""))
				.andExpect(xpath("/CreateFriendResponse/data/country").string(""))
				.andExpect(xpath("/CreateFriendResponse/data/userId").string("2"))
				.andReturn();
		
		//SignUp
		RequestBuilder requestBuilder2 = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>Gaurav Pandarkar</fullName>\n"
						+ "<email>gaurav@gmail.com</email>\n"
						+ "<password>Gaurav@123</password>\n" 
						+ "<mobile>4444444444</mobile>\n"
						+ "<currency>Dollar</currency>\n" 
						+ "<language>English</language>\n" 
						+ "<country>US</country>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
	
		MvcResult result2 = mocMvc.perform(requestBuilder2).andExpect(status().isCreated())
				.andExpect(xpath("/SignUpResponse/status").string("success"))
				.andExpect(xpath("/SignUpResponse/message").string("user account created successfully"))
				.andExpect(xpath("/SignUpResponse/data/fullName").string("Gaurav Pandarkar"))
				.andExpect(xpath("/SignUpResponse/data/email").string("gaurav@gmail.com"))
				.andExpect(xpath("/SignUpResponse/data/mobile").string("4444444444"))
				.andExpect(xpath("/SignUpResponse/data/currency").string("Dollar"))
				.andExpect(xpath("/SignUpResponse/data/language").string("English"))
				.andExpect(xpath("/SignUpResponse/data/country").string("US"))
				.andExpect(xpath("/SignUpResponse/data/userId").string("2"))
				.andReturn();
	}
}